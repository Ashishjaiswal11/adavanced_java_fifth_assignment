package com.nagarro.authorbook.dao;

import java.util.List;


import com.nagarro.authorbook.model.Book;

public interface IBookDao {

	public List<Book> getAllData();
	public Book getOneData(int id);
	public void insertBook(Book book);
	public void delete(int id) throws Exception;
	public void update(Book book) throws Exception;
	
}
