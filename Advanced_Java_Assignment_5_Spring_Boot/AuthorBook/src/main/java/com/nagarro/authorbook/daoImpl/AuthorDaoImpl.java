package com.nagarro.authorbook.daoImpl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.nagarro.authorbook.dao.IAuthorDao;
import com.nagarro.authorbook.model.Author;
import com.nagarro.authorbook.util.HibernateUtil;


@Repository
public class AuthorDaoImpl  implements IAuthorDao{
	
	@Override
	public List<Author> getAuthor() {
		List<Author> author=null;		  
				try(Session ses=HibernateUtil.getSession()){					
					 Transaction tx=ses.beginTransaction(); 			
					 author=ses.createQuery(" FROM Author ").getResultList();
				if(author!=null)
				    return author;
				else
					return null;
				}
				catch(HibernateException he) {
					he.printStackTrace();
					throw he;
				}
			}

	

}
