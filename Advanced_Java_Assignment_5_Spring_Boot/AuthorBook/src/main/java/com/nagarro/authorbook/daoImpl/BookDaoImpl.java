package com.nagarro.authorbook.daoImpl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.nagarro.authorbook.dao.IBookDao;
import com.nagarro.authorbook.model.Book;
import com.nagarro.authorbook.util.HibernateUtil;

@Repository
public class BookDaoImpl  implements IBookDao {

	
	
	 	
		@Override
		public List<Book> getAllData() {
			List<Book> book=null;
			try(Session ses=HibernateUtil.getSession()){							
				 Transaction tx=ses.beginTransaction(); 
		
book=  ses.createQuery(" FROM Book ").getResultList();
tx.commit();
			}		
			catch(HibernateException he) {
				he.printStackTrace();
				
			}
			return book;
		}
	 	
	
	@Override
	public void insertBook(Book book) {
		  
				try(Session ses=HibernateUtil.getSession()){
					Transaction tx=ses.beginTransaction();
													
					 ses.merge(book);
					 tx.commit();
				}
				catch(HibernateException he) {
					he.printStackTrace();				
				}
			}
		 	
		
		
	


	@Override
	public void delete(int id) throws Exception {
		  
				try(Session ses=HibernateUtil.getSession()){
					Transaction tx=ses.beginTransaction();
            Book book = ses.load(Book.class, id);         
    		    ses.delete(book); 
    		    tx.commit();
    		}
				catch(HibernateException he) {
					he.printStackTrace();					
				}
			}
		 	
	


	@Override
	public void update(Book book) throws Exception {
		 
		try(Session ses=HibernateUtil.getSession()){	
			Transaction tx=ses.beginTransaction();
			 ses.saveOrUpdate(book);
			 tx.commit();
		}
		catch(HibernateException he) {
			he.printStackTrace();			
		}
	}


	@Override
	public Book getOneData(int id) {
		 Book book=null;
		  
			try(Session ses=HibernateUtil.getSession()){	
				Transaction tx=ses.beginTransaction();
           book = ses.get(Book.class, id);
           //tx.commit();
							
			}
			catch(HibernateException he) {
				he.printStackTrace();			
			}
			return book;
	}




}
