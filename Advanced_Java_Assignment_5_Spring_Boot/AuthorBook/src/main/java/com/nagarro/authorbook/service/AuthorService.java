package com.nagarro.authorbook.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.nagarro.authorbook.model.Author;

/*
 * Author service interface 
 */

@Service
public interface AuthorService {
	
	public List<Author> getAuthor();
}
