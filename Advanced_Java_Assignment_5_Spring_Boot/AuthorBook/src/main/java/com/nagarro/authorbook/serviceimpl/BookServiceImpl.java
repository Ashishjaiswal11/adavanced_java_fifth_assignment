package com.nagarro.authorbook.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.authorbook.daoImpl.BookDaoImpl;
import com.nagarro.authorbook.model.Book;
import com.nagarro.authorbook.service.AuthorService;
import com.nagarro.authorbook.service.BookService;


@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	BookDaoImpl bookDao;
	
	@Autowired
	AuthorService authorService;
	
	public List<Book> getAllData() {
		
		return bookDao.getAllData();
	}

	public void delete(int id) throws Exception {
		bookDao.delete(id);
		
	}

	public void update(Book book) throws Exception  {
		bookDao.update(book);
	}

	public void insertBook(Book book) {
		bookDao.insertBook(book);
	}

	@Override
	public Book getOneData(int id) {
		
		return bookDao.getOneData(id);
	}

}
