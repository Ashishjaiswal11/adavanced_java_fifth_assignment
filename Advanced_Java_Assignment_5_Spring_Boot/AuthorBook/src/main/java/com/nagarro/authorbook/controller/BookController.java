package com.nagarro.authorbook.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.authorbook.model.Author;
import com.nagarro.authorbook.model.Book;
import com.nagarro.authorbook.serviceimpl.AuthorServiceImpl;
import com.nagarro.authorbook.serviceimpl.BookServiceImpl;


@RestController
public class BookController {

	@Autowired
	BookServiceImpl bookServiceImpl;
	
	@Autowired
	AuthorServiceImpl authorServiceImpl;
	
	
	@GetMapping("/books")
	public ResponseEntity<List<Book>> getBooks() { 
		List<Book> allData = bookServiceImpl.getAllData();
		
		if (allData != null && allData.size() > 0) {
			return new ResponseEntity<>(allData,HttpStatus.OK);
		}else {
			return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@GetMapping("/books/{id}")
	public ResponseEntity<Book> getOneBook(@PathVariable("id") int id) {
		System.out.println("id is "+id);
     	Book data = bookServiceImpl.getOneData(id);   	
		if (data != null) {
			return new ResponseEntity<Book>(data,HttpStatus.OK);
		}else {
			return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/books")
	public void postHome(@RequestBody Book book) {	
		bookServiceImpl.insertBook(book);
	}
	
	
	@DeleteMapping("/books/{id}")
	public ResponseEntity delete(@PathVariable("id") int id) {
		try {
			bookServiceImpl.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>("No book Found with given id", HttpStatus.BAD_REQUEST);

		}
	}
	
	
	@PutMapping("/books/{id}")
	public ResponseEntity<Object> update(@RequestBody Book bookData,@PathVariable("id") int id) {
		try {
			Book book = new Book();
			book.setBookId(bookData.getBookId());
			book.setBookName(bookData.getBookName());	
			book.setAuthorName(bookData.getAuthorName());
			book.setCurrentDate(bookData.getCurrentDate());
			bookServiceImpl.update(book);
			
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			
			 return new ResponseEntity<>("No book Found with given id",HttpStatus.BAD_REQUEST);
		}
	}
	

	@GetMapping("/author")
	public List<Author> getAuthor() {
		return authorServiceImpl.getAuthor();
	}
	
}
