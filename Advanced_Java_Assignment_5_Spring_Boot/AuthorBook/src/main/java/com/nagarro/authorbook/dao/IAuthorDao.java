package com.nagarro.authorbook.dao;

import java.util.List;

import com.nagarro.authorbook.model.Author;

public interface IAuthorDao {

	public  List<Author> getAuthor();

}
