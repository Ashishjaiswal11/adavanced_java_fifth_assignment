package com.nagarro.authorbook.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.authorbook.daoImpl.AuthorDaoImpl;
import com.nagarro.authorbook.model.Author;
import com.nagarro.authorbook.service.AuthorService;


@Service
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	AuthorDaoImpl authorDao;

	@Override
	public List<Author> getAuthor() {
		
		return authorDao.getAuthor();
	}
	
	

}
