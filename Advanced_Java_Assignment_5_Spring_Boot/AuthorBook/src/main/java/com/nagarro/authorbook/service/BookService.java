package com.nagarro.authorbook.service;

import java.util.List;

import org.springframework.stereotype.Service;


import com.nagarro.authorbook.model.Book;

/*
 * Book service Interface
 */

@Service
public interface BookService {
	
	public List<Book> getAllData();
	public Book getOneData(int id);
	public void delete(int id) throws Exception;
	public void update(Book book) throws Exception;
	public void insertBook(Book book);
}
