package com.nagarro.librarian.service;

import org.springframework.stereotype.Service;

import com.nagarro.librarian.entity.User;

@Service
public interface LoginService {

	public User saveUserData(User user);
}
