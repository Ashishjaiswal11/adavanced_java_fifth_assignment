package com.nagarro.librarian.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.nagarro.librarian.entity.User;
import com.nagarro.librarian.util.HibernateUtil;

@Component
public class UserDao {

 static	SessionFactory factory;
	 
	 static	Session ses;

	 Transaction tx=null;
	 	
	 	static {
	 		 factory=HibernateUtil.getSessionFactory();
	 		 ses=HibernateUtil.getSession();
	 	}

	public User saveUserData(User user) {
		tx=ses.beginTransaction();
	 System.out.println("save value are "+ses.save(user));  
	 tx.commit();
	 System.out.println("get values are "+ses.get(User.class,user.getId()));
		 return ses.get(User.class,user.getId());
	}
}
