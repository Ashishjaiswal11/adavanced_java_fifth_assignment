package com.nagarro.librarian.serviceImpl;

import java.util.List;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nagarro.librarian.model.Book;
import com.nagarro.librarian.service.BookService;

@Component
public class BookServiceImpl implements BookService {

	private RestTemplate restTemplate = new RestTemplate();
	

	@Override
	public Book[] getBook() {
		String path="http://localhost:8084/AuthorBook/books";
		ResponseEntity<Book[]> bookArray=restTemplate.exchange(path,HttpMethod.GET,null ,Book[].class);
		Book[] book = bookArray.getBody();
		return book;
	}

	@Override
	public void addBook(Book book) {
		String path="http://localhost:8084/AuthorBook/books";
		restTemplate.postForEntity(path, book, Book.class);
	}

	@Override
	public void updateBook(int id) {
		String path="http://localhost:8084/AuthorBook/books";
		restTemplate.put(path+id,Book.class);
	}

	@Override
	public void deleteBook(int id) {
		String path="http://localhost:8084/AuthorBook/books/";
		restTemplate.delete(path+id);

	}

	@Override
	public Book getBookById(int id) {
		String path="http://localhost:8084/AuthorBook/books/"+id;
		ResponseEntity<Book> book=restTemplate.exchange(path,HttpMethod.GET,null ,Book.class);
		return book.getBody();
	}

}
