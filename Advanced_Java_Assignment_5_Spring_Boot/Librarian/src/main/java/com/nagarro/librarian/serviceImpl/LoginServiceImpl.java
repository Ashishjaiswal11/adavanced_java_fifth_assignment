package com.nagarro.librarian.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nagarro.librarian.dao.UserDao;
import com.nagarro.librarian.entity.User;
import com.nagarro.librarian.service.LoginService;

@Component
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserDao userDao;
	
	@Override
	public User saveUserData(User user) {
	return	userDao.saveUserData(user);
		
	}
	
}
