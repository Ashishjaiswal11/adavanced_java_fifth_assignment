package com.nagarro.librarian.service;

import com.nagarro.librarian.model.Author;

public interface GetAuthorService {

	public Author[] getAuthor();
}
