package com.nagarro.librarian.controller;



import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nagarro.librarian.entity.User;
import com.nagarro.librarian.model.Book;
import com.nagarro.librarian.service.BookService;
import com.nagarro.librarian.service.LoginService;

@Controller
public class LoginController {
	
	
	
	@Autowired
	private LoginService loginService;

	@Autowired
	BookService bookService;

	private String userId;
	
	@GetMapping("/")
	public String loginPage() {
		return "login";
	}
	
//	@GetMapping("/login")
//	public String canclePage() {
//		return "libraryPage";
//	}


	
	@PostMapping("/login")
	public String Login(Map<String,Object> map,@ModelAttribute("user") User user) throws Exception {

		
		User usr=loginService.saveUserData(user);
		  userId=usr.getUserId(); 
			map.put("uname", userId);
			Book[] book = bookService.getBook();
//			for(Book b : book) {
//				System.out.println("AUthor is " + b.getAuthorName());			
//			}
			map.put("bookArray", book);
			return "libraryPage";
		
	}
	
	

	@PostMapping("/logout")
	public String logout() {
		return "login";
	}
	
	@GetMapping("/addBook")
	public String addBookList() {
		return "addBook";
	}

	
	@PostMapping("/addBook")
	public String addBook(Map<String,Object> map,@ModelAttribute("book") Book book) {
		
		bookService.addBook(book);
		map.put("uname",userId);
		Book[] b=bookService.getBook();
		map.put("bookArray",b);
		return "libraryPage";
	}
	
	
	@RequestMapping("/deleteBook")
	public String deleteBook(Model model,@RequestParam("bookCode") int bookCode) {
		bookService.deleteBook(bookCode);
		model.addAttribute("uname",userId);
		Book[] book=bookService.getBook();
		model.addAttribute("bookArray",book);
		return "libraryPage";
	}
	
	@GetMapping("/editBook")
	public String editBookList(Map<String,Object> map,@RequestParam("id") int id) {

		Book book=bookService.getBookById(id);
		map.put("details",book);
		return "editBook";
	}
	

	

}
