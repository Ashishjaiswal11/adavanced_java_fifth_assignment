package com.nagarro.librarian.service;

import org.springframework.stereotype.Service;

import com.nagarro.librarian.model.Book;

@Service
public interface BookService {

	public Book[] getBook();
	
	public Book getBookById(int id);

	public void addBook(Book book);

	public void updateBook(int id);

	public void deleteBook(int id);
}
