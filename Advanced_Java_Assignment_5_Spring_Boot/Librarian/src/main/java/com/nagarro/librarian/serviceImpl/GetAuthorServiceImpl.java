package com.nagarro.librarian.serviceImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nagarro.librarian.model.Author;
import com.nagarro.librarian.service.GetAuthorService;


@Component
public class GetAuthorServiceImpl implements GetAuthorService{

	@Override
	public Author[] getAuthor() {
		RestTemplate restTemplate=new RestTemplate();
		ResponseEntity<Author[]> responseEntity=restTemplate.getForEntity("http://localhost:8084/AuthorBook/author",Author[].class);
		return responseEntity.getBody();
	}

}
